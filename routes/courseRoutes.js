const express = require("express");
const router = express.Router();
const CourseController = require("../controllers/courseControllers");

const auth = require("../auth");

// creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin) {
		CourseController.addCourse(data).then(result => res.send(result));
	} else {
		res.send({ auth: "You're not an admin"})
	}
})


// activity Sir Orlandos answer
// router.post("/", auth.verify, (req, res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	isAdmin = userData.isAdmin;
// 	console.log(isAdmin);

// 	if(isAdmin) {
// 		CourseController.addCourse(req.body).then(result => res.send(result));
// 	}else {
// 		res.send(false);
// 	}
	
// })


// retrieving all courses
router.get("/all", (req, res) => {
	CourseController.getAllCourses().then(result => res.send(result));
})


// retrieving all active courses
router.get("/", (req, res) => {
	CourseController.getAllActive().then(result => res.send(result))
});



// retrieving a specific course
router.get("/:courseId", (req, res) => {
	console.log(req.params.courseId);
	CourseController.getCourse(req.params.courseId).then(result => res.send(result))
})


//1 update a course
router.put("/:courseId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin){
		CourseController.updateCourse(req.params.courseId, req.body).then(result => res.send(result));		
	} else {
		res.send(false);
	}
})


/* Activity
1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.
2. Create a controller method for archiving a course obtaining the course ID changing the isActive true to false
3. Process a PUT request at the /courseId/archive route using postman to archive a course
4. Create a git repository named S35.
5. Add another remote link and push to git with the commit message of Add activity code - S35 Activity.
6. Add the link in Boodle named "Express js API Development Part 4".
*/

router.put("/:id/archive", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		CourseController.archiveCourse(req.params.courseId).then(result => res.send(result))
	} else {
		res.send(false);
	}

})







module.exports = router;
