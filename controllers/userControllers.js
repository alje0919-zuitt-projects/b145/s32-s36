const User = require("../models/User");

const Course = require("../models/Course");

// encrypted password
const bcrypt = require('bcrypt');

const auth = require('../auth');

// check if the email already exists
/*
1. use mongoose "find" method to find duplicate emails
2. use the then method to send a response back to the client

*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email: reqBody.email }).then(result => {

		// if match is found
		if(result.length > 0){
			return true;
		} else {
			// no duplicate email found
			// user is not yet registered in db
			return false;
		}
	})
}


// User Registration
/*
1. create a new User object
2. make sure that the password is encrypted
3. save the new User to db
*/

/*
Function parameters from routes to controllers

Routes (argument)
*/
module.exports.registerUser = (reqBody) => {

	// creates a new User Object
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		mobileNo: reqBody.mobileNo,
		email: reqBody.email,
		// password: reqBody.password
		// 10 is the value provided as the number of 'salt' round that the bcrypt algo will run in order to encrypt the password
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	// saves the created object to db
	return newUser.save().then((user, error) => {
		if(error) {
			return false;
		} else {
			// user registration is success
			return true;
		}
	})

}

// User authentication
/*
Steps:
1. check the db if the user email exists
2. compare password provided in the login form with the password stored in db.
3. generate/return a JSON web token if user successfully logged in and return false if not.

*/

module.exports.loginUser = (reqBody) => {
	// findOne will return the first record in the collection that matches the search criteria
	return User.findOne({ email: reqBody.email }).then(result => {
		// user does not exist
		if(result == null){
			return false;
		} else {
			// user exists
			// the 'compareSync' method is used to compare a non-encrypted password from the login form to the encrypted password retrieved from the databse and returns 'true' or 'false'
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			// if the password match
			if(isPasswordCorrect){
				// generate an access token
				return { accessToken : auth.createAccessToken(result.toObject())}
			} else {
				// password does not match
				return false;
			}
		}
	})
}


// Activity

/*
1. find the doc using the users ID
2. reassign the pw of the returned doc to an empty string
3. return the result back to the client

*/

module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = "";
		return result;
	})
}

/*
module.exports.getProfile = (data) => {
	return User.findById(data).then(result => {
		result.password = "";
		return result;
	})
}
*/


// Enroll user to a course
/*
Steps:
1. find the document in the database using the user's ID
2. add the courseID to the user's enrollment array using the push method.
3. add the userId to the course's enrollees arrays.
4. validation
5. save the document
*/

// Async and await - allow the processes to wait for each other

module.exports.enroll = async (data) => {
	// add the courseId to the enrollments array of the user
	let isUserUpdated = await User.findById(data.userId).then( user => {
		// push the course Id to enrollments property

		user.enrollments.push({ courseId: data.courseId });

		// save
		return user.save().then((user, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// add the userId in the course's database(enrollees)
		course.enrollees.push({ userId: data.userId });

		return course.save().then((course, error) => {
			if(error) {
				return false;
			} else {
				return true;
			}
		})
	})

	// validation
	if(isUserUpdated && isCourseUpdated){
		return true;
	} else {
		// user enrollment failed
		return false;
	}

}
