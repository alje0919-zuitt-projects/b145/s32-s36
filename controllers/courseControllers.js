const Course = require("../models/Course");

// create a new course
/*
Steps:
1. create a new course object
2. save to db
3. error handling

*/

// module.exports.addCourse = (reqBody) => {
// 	console.log(reqBody);

// 	// create a new object
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});

// 	// saves the created object to our database
// 	return newCourse.save().then((course, error) => {
// 		// course creation failed
// 		if(error) {
// 			return false;
// 		}else {
// 			// course creation successful
// 			return true;
// 		}
// 	})
// }

module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	// create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	// saves the created object to our database
	return newCourse.save().then((course, error) => {
		// course creation failed
		if(error) {
			return false;
		}else {
			// course creation successful
			return true;
		}
	})
}


// retrieving all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result;
	})
}

// retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({ isActive: true}).then(result => {
		return result;
	})
}

// retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}

//2 update a course
/*
steps:
1. create variable which will contain the information retrieved from the request body
2. find and update course using the course ID
*/

module.exports.updateCourse = (courseId, reqBody) => {
	// specify the properties of the doc to be updated
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	// findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
		// course not updated
		if(error) {
			return false;
		}else {
			// course updated successfully
			return true;
		}
	})
}



// Activity


module.exports.archiveCourse = (reqParams) => {
	let updateActiveField = {
		isActive: false
	}
	return Course.findByIdAndUpdate(reqParams, updateActiveField).then((course, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}